from fastapi import FastAPI
from joblib import load
from fastapi.responses import JSONResponse
from preprocess import preprocess  # Assuming preprocess is a custom module you've created
import os
from dotenv import load_dotenv
from datetime import datetime

load_dotenv()

app = FastAPI()

# Load your model
model = load('assets/Logistic Regression_v1.0.0.joblib')
classes = ['negative', 'positive']

uncertainty_threshold = float(os.getenv('UNCERTAINTY_THRESHOLD'))  # Adjust as needed

@app.get("/")
async def predict(text: str):
    vector = preprocess(text)
    probs = model.predict_proba(vector)[0]  # Get prediction probabilities
    pred_index = probs.argmax()

    prediction = classes[pred_index]
    confidence = probs[pred_index]

    is_uncertain = confidence < uncertainty_threshold
    is_uncertain = bool(is_uncertain)  # Explicit conversion to Python bool

    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    response = {
        "text": text,
        "prediction": prediction,
        "confidence": float(confidence),  # Convert to Python float for JSON serialization
        "is_uncertain": is_uncertain,
        "timestamp": timestamp  # Include the formatted timestamp in the response
    }

    return JSONResponse(content=response, status_code=200)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host=os.getenv('HOST'), port=os.getenv('PORT'))