import re
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import nltk
import pickle

# Download model dan vectorizer dari neptune.
# [v] nilai + jika model management nya pakai neptune
# [v] nilai + preprocessor/vectorizer pakai neptune
# Jika tidak ada model dan vectorizer berformat .joblib di dalam assets, 
# maka akan otomatis melakukan download sesuai dengan run_id di neptune.ai.
import os
import neptune

from dotenv import load_dotenv
import joblib
import yaml

# Load methods config from YAML
with open("config.yaml", "r") as file:
    config = yaml.safe_load(file)

run = neptune.init_run(project=os.getenv('NEPTUNE_PROJECT_NAME'), with_id=config['run_id'], mode='read-only')

# Model
choosen_method = config['choosen_method']
model_version = config['model_version']
model_filename = f"{choosen_method}_{model_version}.joblib"
model_namespace = f"models/{choosen_method}/{model_version}"
model_neptune_location = f"{model_namespace}/{model_filename}"

# Vectorizer
choosen_vectorizer_method = config['choosen_vectorizer_method']
vectorizer_version = config['vectorizer_version']
vectorizer_filename = f"{choosen_vectorizer_method}_{vectorizer_version}.joblib"
vectorizer_namespace = f"vectorizers/{choosen_vectorizer_method}/{vectorizer_version}"
vectorizer_neptune_location = f"{vectorizer_namespace}/{vectorizer_filename}"

local_vectorizer_path = f"assets/{vectorizer_filename}"
local_model_path = f"assets/{model_filename}"

# Check model existence
if not os.path.exists(local_model_path):
    print("Model not found. Downloading from neptune.ai...")
    try:
        # Download model
        run[model_neptune_location].download(destination=local_model_path)  
        print("Model download complete!")
    except Exception as e:
        print(f"Error downloading model: {e}")
else:
    print("Model already exists. Skipping download.")

# Check vectorizer existence
if not os.path.exists(local_vectorizer_path):
    print("Vectorizer not found. Downloading from neptune.ai...")
    try:
        # Download vectorizer
        run[vectorizer_neptune_location].download(destination=local_vectorizer_path)  
        print("Vectorizer download complete!")
    except Exception as e:
        print(f"Error downloading vectorizer: {e}")
else:
    print("Vectorizer already exists. Skipping download.")

run.stop()

# Coba Preprocess
nltk.download('stopwords')

port_stem = PorterStemmer()

def stemming(content):
    stemmed_content = re.sub('[^a-zA-Z]', " ", content)   # the regular expression matches any pattern that is not a character
                                                          # (since negation ^ is used) and replaces those matched sequences 
                                                          # with empty space, thus all special characters and digits get 
                                                          # removed.
    stemmed_content = stemmed_content.lower()
    stemmed_content = stemmed_content.split()
    stemmed_content = [port_stem.stem(word) for word in stemmed_content if word not in stopwords.words('english')]   
                                                          # apply port_stem only on words not in the list of stop-words
    stemmed_content = " ".join(stemmed_content)
    
    return stemmed_content

vectorizer = joblib.load(local_vectorizer_path)

def preprocess(text):
    stemmed = stemming(text)
    vector = vectorizer.transform([stemmed])
    return vector

if __name__ == '__main__':
    print(preprocess('that food is so good'))