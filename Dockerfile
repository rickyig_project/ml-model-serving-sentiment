FROM python:3.10.12

WORKDIR /app

ADD . .

RUN pip install -r requirements.txt

ENV NEPTUNE_PROJECT_NAME="rickyig/sentiment-analysis"
ENV NEPTUNE_API_TOKEN="eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vYXBwLm5lcHR1bmUuYWkiLCJhcGlfdXJsIjoiaHR0cHM6Ly9hcHAubmVwdHVuZS5haSIsImFwaV9rZXkiOiJkNDQzNzIyYi0zMTM5LTQ4NGEtYWIxZC1jYTAwMjY0YmZlYTcifQ=="

RUN python preprocess.py

CMD ["python", "app.py"]
