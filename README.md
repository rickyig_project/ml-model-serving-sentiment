# Data Processing Pipeline for AI Projects

Welcome to our AI project's repository! This README provides a detailed guide on our data processing pipeline, ensuring clarity and reproducibility in our workflows. Here, you will find step-by-step instructions on how we handle data cleaning, feature extraction, feature engineering, data splitting, versioning, modeling, MLOps - Neptune.ai, and Model Serving.

## Table of Contents
- [Project Overview](#project-overview)
- [Data Processing Pipeline](#data-processing-pipeline)
  - [Data Cleaning](#data-cleaning)
  - [Feature Extraction](#feature-extraction)
  - [Feature Engineering](#feature-engineering)
  - [Data Splitting](#data-splitting)
  - [Data Versioning](#data-versioning)
  - [Modeling](#modeling)
  - [MLOps - Neptune.ai](#mlops---neptuneai)
  - [Model Serving](#model-serving)
- [Getting Started](#getting-started)
- [Contributing](#contributing)
- [License](#license)

## Project Overview
Briefly describe the goals and objectives of your AI project. Include any relevant context or links to resources that provide additional information.

## Data Processing Pipeline

### Data Cleaning & Feature Extraction & Feature Engineering & Data Splitting
**Objective:** Ensure that the data is free of errors or inconsistencies, and is ready for analysis and modeling.

**Steps:**
1. **Stemming**
2. **TF-IDF**

**Tools & Libraries:**
- Pandas for data manipulation
- NumPy for numerical operations
- Scikit-learn for preprocessing


### Data Versioning
**Objective:** Keep track of different versions of datasets used in the project.

**Steps:**
1. **Version Control:** Use tools like DVC to manage and version control datasets.
2. **Documentation:** Document the changes in each version, including the rationale for changes and impact on models.

**Tools & Libraries:**
- GitLab

### Modeling
**Objective:** Create a machine learning model. Model used: Logistic Regression, Random Forest, SVC

**Tools & Libraries:**
- Scikit-learn

### MLOps - Neptune.ai
**Objective:** Neptune is a metadata store for MLOps, built for research and production teams that run a lot of experiments. It gives you a central place to log, store, display, organize, compare, and query all metadata generated during the machine learning lifecycle.

**Tools & Libraries:**
- Neptune.ai

## Getting Started
Provide instructions on how to set up and run the project locally. This section should include:
- Installation of required libraries
- How to run the scripts
- Example commands

```bash
# Clone the repository
git clone https://github.com/your-username/your-project-name.git
cd your-project-name

# Install dependencies
pip install -r requirements.txt

# Run the data processing script
python data_processing.py

# Run the data train script
python train.py
```

## Experimented on Neptune.ai
Contohnya ketika menggunakan <b>Logistic Regression</b>
<div align="center">
  <img src="assets/img/SEN2.png" alt="Sen2" width="1920" height="1080">
</div>

## Model Serving
- Model Version: v1.0.0
- Data Version: v1.0.0
- Vectornizer Version: v1.0.0
- Run_ID: SEN-3

- Data Management System Link: https://gitlab.com/rickyig_project/sentiment-data-management-system 
- Model Management System Link: https://gitlab.com/rickyig_project/ml-modeling-sentiment
- Docker Image Repository : https://hub.docker.com/repository/docker/rickyindrag/rickyindrag_sentiment/general

### How to set up :

Via docker:

`docker image pull rickyindrag/rickyindrag_sentiment:latest`

Via local:

`git clone https://gitlab.com/rickyig_project/ml-model-serving-sentiment`

`pip install requirements.txt`


### How to run :

**Docker**

```docker run --name rickyindrag_sentiment -e UNCERTAINTY_THRESHOLD=0.5 -e HOST=0.0.0.0 -e PORT=8080 -p 8080:8080 -d rickyindrag_sentiment:latest```


**Local**

`python app.py`

then **visit** `localhost:8080/docs`

### Include:

- [x] Nilai + jika model management nya pakai neptune [Done]
- [x] Nilai + preprocessor/vectorizer pakai neptune [Done]

### Contoh Demo:
<div align="center">
  <img src="assets/img/sentimen8.png" alt="sentimen8" width="1920" height="1080">
</div>
<div align="center">
  <img src="assets/img/sentimen9.png" alt="sentimen9" width="1920" height="1080">
</div>

## Contributing
Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License
Specify the license under which your project is made available. This informs users of what they can and cannot do with your code.
